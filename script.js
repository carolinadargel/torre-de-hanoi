const altura = "40px";

let body;
let movimentos = 0;
let pino1 = new Pino(true);
let pino2 = new Pino(false);
let pino3 = new Pino(false);
let discoselecionado;
let origem;
let destino;

function criarDiv() {
    let box1 = document.createElement("div");
    return box1;
}

function over1() {
    over(pino1);
}
function over2() {
    over(pino2);
}
function over3() {
    over(pino3);
}
function over(div) {
    div.box1.style.backgroundColor = "#FFC1C1";
}

function out1() {
    out(pino1);
}
function out2() {
    out(pino2);
}
function out3() {
    out(pino3);
}
function out(div) {
    div.box1.style.backgroundColor = "#fff";
}
function click1() {
    pino1.selecionado = !pino1.selecionado;
    click(pino1);
}
function click2() {
    pino2.selecionado = !pino2.selecionado;
    click(pino2);
}
function click3() {
    pino3.selecionado = !pino3.selecionado;
    click(pino3);
}
function click(pino) {
    if (pino.selecionado) {
        selecionarOrigemDestino(pino);
    } else {
        pino.box1.style.borderColor = "black";
        reiniciarOrigemDestino();
    }
}

function preencherConteudo() {
    let conteudo = new Array();
    for (var i = 0; i < 5; i++) {
        conteudo[i] = new Cheio();
    }
    return conteudo;
}
function preencherDiscos() {
    let conteudo = new Array();
    conteudo[0] = new Cheio();
    conteudo[1] = new Disco1();
    conteudo[2] = new Disco2();
    conteudo[3] = new Disco3();
    conteudo[4] = new Disco4();
    return conteudo;
}
function Disco1() {
    this.box1 = criarDiv();
   this.box1.style.width = "14%";
   this.box1.style.height = altura;
   this.box1.style.backgroundColor = "#C71585";
   this.box1.style.marginLeft = "auto";
   this.box1.style.marginRight = "auto";
   this.box1.style.borderRadius = "10px 10px "
   this.valor = 0;

}
function Disco2() {
    this.box1 = criarDiv();
   this.box1.style.width = "30%";
   this.box1.style.height = altura;
   this.box1.style.backgroundColor = "#DB7093";
   this.box1.style.marginLeft = "auto";
   this.box1.style.marginRight = "auto";
   this.box1.style.borderRadius = "10px 10px"
   this.valor = 1;
}
function Disco3() {
    this.box1 = criarDiv();
    this.box1.style.width = "50%";
    this.box1.style.height = altura;
    this.box1.style.backgroundColor = "#F08080";
    this.box1.style.marginLeft = "auto";
    this.box1.style.marginRight = "auto";
    this.box1.style.borderRadius = "10px 10px"
    this.valor = 2;
 }
function Disco4() {
    this.box1 = criarDiv();
    this.box1.style.width = "70%";
    this.box1.style.height = altura;
    this.box1.style.backgroundColor = "#800000";
    this.box1.style.marginLeft = "auto";
    this.box1.style.marginRight = "auto";
    this.box1.style.borderRadius = "10px 10px"
    this.valor = 3;
}
function Cheio() {
    this.box1 = criarDiv();
    this.box1.style.width = "100%";
    this.box1.style.height = altura;
}
function Pino(box1Inicial) {
    this.box1 = criarDiv();
    this.box1.style.width = "28%";
    this.box1.style.height = "200px";
    this.box1.style.marginLeft = "4%";
    this.box1.style.borderWidth = "2%";
    this.box1.style.border = "solid black";
    this.box1.style.float = "left";
    this.selecionado = false;
    this.conteudo;
    if (box1Inicial) {
        this.conteudo = preencherDiscos();
    } else {
        this.conteudo = preencherConteudo();
    }
    for (let i = 0; i < this.conteudo.length; i++) {
        this.box1.appendChild(this.conteudo[i].box1);
    }
    this.temdiscos = function () {
        let conteudos = 0;
        for (let i = 0; i < this.conteudo.length; i++) {
            if (this.conteudo[i] instanceof Cheio) {
                conteudos++;
            }
        }
        if (conteudos == this.conteudo.length) {
            return false;
        } else {
            return true;
        }
    };
    this.PegaDiscoSuperior = function () {
        for (let i = 0; i < this.conteudo.length; i++) {
            if (!(this.conteudo[i] instanceof Cheio)) {
                return this.conteudo[i];
            }
        }
    };
    this.tirardiscoSuperior = function () {
        for (var i = 0; i < this.conteudo.length; i++) {
            if (!(this.conteudo[i] instanceof Cheio)) {
                discoselecionado = this.conteudo[i];
                this.conteudo[i] = new Cheio();
                break;
            }
        }
    };
    this.InserirdiscoSuperior = function () {
        for (var i = this.conteudo.length - 1; i >= 0; i--) {
            if (this.conteudo[i] instanceof Cheio) {
                this.conteudo[i] = discoselecionado;
                break;
            }
        }
    };
    this.RedesenhaBox = function () {
        while (this.box1.hasChildNodes()) {
            this.box1.removeChild(this.box1.lastChild);
        }
        for (var i = 0; i < this.conteudo.length; i++) {
            this.box1.appendChild(this.conteudo[i].box1);
        }
    };
}
function selecionarOrigemDestino(div) {
    if (origem == undefined) {
        if (div.temdiscos()) {
            div.box1.style.borderColor = "#9370DB";
            origem = div;
            origem.selecionado = true;
        }
    } else if (origem != undefined && destino == undefined) {
        destino = div;
        destino.selecionado = true;
        if (origem != destino) {
            if (!destino.temdiscos() || (origem.PegaDiscoSuperior().valor < destino.PegaDiscoSuperior().valor)) {
                origem.tirardiscoSuperior();
                origem.RedesenhaBox();
                destino.InserirdiscoSuperior();
                destino.RedesenhaBox();
                movimentos++;
                atualizarContador();
            }
        }
    }
    if (destino != undefined && origem != undefined) {
        reiniciarOrigemDestino();
    }
    if (comprovarVitoria()) {
        vitoria();
    }
}

function comprovarVitoria() {
    if (pino3.conteudo[0] instanceof Cheio &&
        pino3.conteudo[1] instanceof Disco1 &&
        pino3.conteudo[2] instanceof Disco2 &&
        pino3.conteudo[3] instanceof Disco3 &&
        pino3.conteudo[4] instanceof Disco4) {
        return true;
    } else {
        return false;
    }
}



function vitoria() {

t = document.getElementsByTagName("h3") 
    
if (t.length > 0) {
    return


} else if (movimentos == 15 && comprovarVitoria() == true) {
        let textoConselho = document.createTextNode("Pressione F5 para jogar de novo.");
        let textoTitulo = document.createTextNode("Parabéns, você completou o jogo com o número mínimo de movimentos!");
        let titulo = document.createElement("h1");
        let conselho = document.createElement("h3");
        titulo.style.color = "red";
        titulo.appendChild(textoTitulo);
        conselho.appendChild(textoConselho);
        body.appendChild(titulo);
        body.appendChild(conselho);


        return titulo.appendChild(textoTitulo) + conselho.appendChild(textoConselho)

    } else if (movimentos > 15 && comprovarVitoria() == true) {
        let textoConselho = document.createTextNode("Pressione F5 para tentar novamente.");
        let textoTitulo = document.createTextNode("Legal, você completou o jogo com " + movimentos + " movimentos, mas sabemos que você pode fazer melhor...");
        let titulo = document.createElement("h1");
        let conselho = document.createElement("h3");
        titulo.style.color = "red";
        titulo.appendChild(textoTitulo);
        conselho.appendChild(textoConselho);
        body.appendChild(titulo);
        body.appendChild(conselho);


        return titulo.appendChild(textoTitulo) + conselho.appendChild(textoConselho)

    }

}



function reiniciarOrigemDestino() {
    if (origem != undefined) {
        origem.box1.style.borderColor = "black";
        origem.selecionada = false;
    }
    if (destino != undefined) {
        destino.box1.style.borderColor = "black";
        destino.selecionado = false;
    }
    origem = undefined;
    destino = undefined;
    pino1.selecionado = false;
    pino2.selecionado = false;
    pino3.selecionado = false;
}
function atualizarContador() {
    var paragrafo = document.getElementById("contador");
    paragrafo.innerHTML = "Movimentos: " + movimentos;
}
function iniciar() {
    body = document.getElementsByTagName("body")[0];
    body.style.textAlign = "center";
    body.appendChild(pino1.box1);
    body.appendChild(pino2.box1);
    body.appendChild(pino3.box1);
    pino1.box1.addEventListener("mouseover", over1, false);
    pino2.box1.addEventListener("mouseover", over2, false);
    pino3.box1.addEventListener("mouseover", over3, false);
    pino1.box1.addEventListener("mouseout", out1, false);
    pino2.box1.addEventListener("mouseout", out2, false);
    pino3.box1.addEventListener("mouseout", out3, false);
    pino1.box1.addEventListener("click", click1, false);
    pino2.box1.addEventListener("click", click2, false);
    pino3.box1.addEventListener("click", click3, false);
    var texto = document.createTextNode("Movimentos: " + movimentos);
    var paragrafo = document.createElement("p");
    paragrafo.style.clear = "both";
    paragrafo.style.paddingTop = "3em";
    paragrafo.setAttribute("id", "contador");
    paragrafo.appendChild(texto);
    body.appendChild(paragrafo);
}
window.addEventListener("load", iniciar, false);

function mostraResposta(id) {
    respostas = document.getElementsByClassName('historia'); //recupera todos elementos da classe faq
    for (var i = 0; i < respostas.length; i++) { // coloca todos eles invisiveis
        respostas[i].style.display = 'none';
    }

    clicada = document.getElementById(id); //recupera o id passado por argumento
    clicada.style.display = 'inherit'; //faz ele ser exibido conforme o item pai
}